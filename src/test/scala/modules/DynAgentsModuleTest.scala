package modules


import models.{Epoch, MachineNode}
import org.scalatest.{BeforeAndAfter, FunSpec, Matchers}
import scalaz.NonEmptyList
import services.WorldView

class DynAgentsModuleTest extends FunSpec with BeforeAndAfter with Matchers {
  import TestFixtures._
  import helpers.Mutable

  describe("business logic") {
    describe("initial") {
      it("generates an initial WorldView") {
        /**
          * This worldview here is created with a full backlog and no agents. Hence the "needsAgents"
          */
        val mutable = new Mutable(needsAgents)
        import mutable._

        program.initial should be(needsAgents)
      }
    }

    describe("update") {
      it("ignore unresponsive pending actions during update") (pending) // I don't understand what they want here

      it("removes changed nodes from pending") {
        val world = WorldView(
          backlog = 0,
          agents = 0,
          managed = managed,
          alive = Map(node1 -> time2),
          pending = Map.empty[MachineNode, Epoch],
          time = time3 // does this matter?
        )
        val mutable = new Mutable(world)
        import mutable._

        // since node1 is pending, it will remove it from the "alive" map
        val old = world.copy(
          alive = Map.empty,
          pending = Map(node1 -> time2),
          time = time2
        )

        program.update(old) should be(world)
      }
    }

    describe("act") {
      it("requests agents when it needs more agents") {
        val mutable = new Mutable(needsAgents) // full backlog with zero alive agents
        import mutable._

        val expected = needsAgents.copy(
          pending = Map(node1 -> time1)
        )

        program.act(needsAgents) should be(expected) //just testing that "pending" has a requested agent in it
        mutable.started should be(1)
        mutable.stopped should be(0)
      }
    }

    describe("as an exercise: add these specs") {
      it("does not request agents when pending") {
        val mutable = new Mutable(alreadyPending)
        import mutable._

        program.act(alreadyPending) should be(alreadyPending)
        mutable.started should be(0)
        mutable.stopped should be(0)
      }

      it("does not shut down agents if nodes are too young") {
        val newNode = WorldView(
          backlog = 0,
          agents = 1,
          managed = managed,
          alive = Map(node1 -> time1),
          pending = Map.empty,
          time = time2
        )

        val mutable = new Mutable(newNode)
        import mutable._

        program.act(newNode) should be(newNode)
        mutable.started should be(0)
      }

      it("shuts down agents when there is no backlog and nodes will shortly incur new costs") {
        val oneNodeStale = WorldView(
          backlog = 0,
          agents = 2,
          managed = managed,
          alive = Map(node1 -> time2, node2 -> time1),
          pending = Map.empty,
          time = time3
        )

        // it doesn't actually remove node2 from the list of alive.... it just stops it and adds it to pending?
        // also it changes the timestamp to be the worldview timestamp
        val expected = oneNodeStale.copy(
          pending = Map(node2 -> time3)
        )

        val mutable = new Mutable(oneNodeStale)
        import mutable._

        program.act(oneNodeStale) should be(expected)
        mutable.started should be(0)
        mutable.stopped should be(1)
      }

      it("does not shut down agents if there are pending actions") {
        val pendingAndStale = WorldView(
          backlog = 1,
          agents = 1,
          managed = managed,
          alive = Map(node1 -> time1), // this is stale
          pending = Map(node2 -> time3),
          time = time3
        )

        val mutable = new Mutable(pendingAndStale)
        import mutable._

        val results = program.act(pendingAndStale)
        results should be(pendingAndStale)
        mutable.started should be(0)
        mutable.stopped should be(0)
      }

      it("shut down agents when there is no backlog if they are too old") {
        val shutOneDown = WorldView(
          backlog = 0,
          agents = 1,
          managed = managed,
          alive = Map(node1 -> time1), // this is stale
          pending = Map.empty,
          time = time3
        )

        val expected = shutOneDown.copy(pending = Map(node1 -> time3))

        val mutable = new Mutable(shutOneDown)
        import mutable._

        val results = program.act(shutOneDown)
        results should be(expected)
        mutable.stopped should be(1)
      }

      it("shut down agents, even if they are potentially doing work, if they are too old") {
        val veryOldNode = WorldView(
          backlog = 1,
          agents = 1,
          managed = managed,
          alive = Map(node1 -> time1), // this is stale
          pending = Map.empty,
          time = time4
        )

        val expected = veryOldNode.copy(
          pending = Map(node1 -> time4)
        )

        val mutable = new Mutable(veryOldNode)
        import mutable._

        val results = program.act(veryOldNode)
        results should be(expected)
        mutable.stopped should be(1)
      }
    }
  }
}

object TestFixtures {
  import helpers.EpochInterpolator.toEpoch

  val node1 = MachineNode("uuid1")
  val node2 = MachineNode("uuid2")
  val managed = NonEmptyList(node1, node2)

  val time1: Epoch = toEpoch("2017-03-03T18:07:00Z")
  val time2: Epoch = toEpoch("2017-03-03T18:59:00Z") // +52 mins
  val time3: Epoch = toEpoch("2017-03-03T19:06:00Z") // +59 mins
  val time4: Epoch = toEpoch("2017-03-03T23:07:00Z") // +5 hours
  val time5: Epoch = toEpoch("2017-03-03T18:09:00Z") // only 2 minutes

  val needsAgents = WorldView(5, 0, managed, Map.empty, Map.empty, time1)
  val alreadyPending = WorldView(5, 0, managed, Map.empty, pending = Map(node1 -> time1), time = time1)
}
