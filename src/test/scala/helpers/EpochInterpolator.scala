package helpers

import java.time.Instant

import models.Epoch

object EpochInterpolator {
  def toEpoch(s: String) = Epoch(Instant.parse(s).toEpochMilli)
}
