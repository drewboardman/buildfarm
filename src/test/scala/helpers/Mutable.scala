package helpers

import models.{Drone, Epoch, MachineNode, Machines}
import modules.DynAgentsModule
import services.WorldView
import scalaz.Scalaz._
import scalaz._

class Mutable(state: WorldView) {
  var started, stopped: Int = 0

  private val D: Drone[Id] = new Drone[Id] {
    def getBacklog: Int = state.backlog
    def getAgents: Int = state.agents
  }

  private val M: Machines[Id] = new Machines[Id] {
    def getAlive: Map[MachineNode, Epoch] = state.alive
    def getManaged: NonEmptyList[MachineNode] = state.managed
    def getTime: Epoch = state.time
    def start(node: MachineNode): MachineNode = {
      started += 1
      node
    }
    def stop(node: MachineNode): MachineNode = {
      stopped += 1
      node
    }
  }

  val program = new DynAgentsModule[Id](D, M)
}
