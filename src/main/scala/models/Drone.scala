package models

import scala.language.higherKinds

/**
In FP, an algebra takes the place of an interface in Java, or the set of valid messages for an Actor
in Akka. This is the layer where we define all side-effecting interactions of our system.

There is tight iteration between writing the business logic and the algebra: it is a good level of
abstraction to design a system.
 */

trait Drone[F[_]] {
  def getBacklog: F[Int]
  def getAgents: F[Int]
}
