package models

import services.WorldView
import scala.language.higherKinds

/**
  *
  * @tparam F
  */

trait DynAgents[F[_]] {
  def initial: F[WorldView]
  def update(old: WorldView): F[WorldView]
  def act(world: WorldView): F[WorldView]
}
