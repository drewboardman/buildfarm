package models

import scala.concurrent.duration._

case class Epoch(millis: Long) extends AnyVal {
  def +(duration: FiniteDuration): Epoch = Epoch(millis + duration.toMillis)
  def -(epoch: Epoch): FiniteDuration = (millis - epoch.millis).millis
}
