package modules

import extractors.{NeedsAgent, Stale}
import models._
import scalaz.Scalaz._
import scalaz._
import services.WorldView
import scala.language.higherKinds

import scala.concurrent.duration._

/**
  * A module depends only on other modules, algebras and pure
  * functions, and can be abstracted over F . If an implementation of an algebraic interface is tied to a
  * specific type, e.g. IO , it is called an interpreter.
  *
  * The Monad context bound means that F is monadic, allowing us to use map , pure and, of course,
  * flatMap via for comprehensions.
  */


final class DynAgentsModule[F[_]: Monad](D: Drone[F], M: Machines[F])
  extends DynAgents[F] {

  override def initial: F[WorldView] = {
    ^^^^(D.getBacklog, D.getAgents, M.getManaged, M.getAlive, M.getTime) {
      case (dbacklog, dagents, mmanaged, malive, mtime) =>
        WorldView(dbacklog, dagents, mmanaged, malive, pending = Map.empty, mtime)
    }
  }

  override def update(old: WorldView): F[WorldView] = for {
    snapshot <- initial
    diff = symdiff(old.alive.keySet, snapshot.alive.keySet)
    pending = removeNodesFromMap(old.pending, diff).filterNot {
      case (_, started) => (snapshot.time - started) >= 10.minutes
    }
    update = snapshot.copy(pending = pending)
  } yield update

  override def act(world: WorldView): F[WorldView] = world match {
    case NeedsAgent(node) =>  // backlog of work AND no agents alive or pending
      for {
        _ <- M.start(node)
        update = world.copy(pending = Map(node -> world.time))
      } yield update

    case Stale(nodes) => // there is no backlog so we can clean up machinenodes
      nodes.foldLeftM(world) { (world, node) =>
        for {
          _ <- M.stop(node)
          update = world.copy(pending = world.pending + (node -> world.time))
        } yield update
      }

    case _ => world.pure[F]
  }

  /**
    * This takes a Map[MachineNode, Epoch] and a Set[MachineNode] and removes the nodes that are in the set from the Map
    * @param old
    * @param diff
    * @return
    */
  private def removeNodesFromMap(pendingNodes: Map[MachineNode, Epoch], diff: Set[MachineNode]): Map[MachineNode, Epoch] = {
    pendingNodes -- diff
  }

  private def symdiff[T](a: Set[T], b: Set[T]): Set[T] =
      (a union b) -- (a intersect b)
}
