package services

import models.{Epoch, MachineNode}
import scalaz.NonEmptyList

/**
  * We need a WorldView class to hold a snapshot of our knowledge of the world. If we were designing
  * this application in Akka, WorldView would probably be a var in a stateful Actor .
  *
  * WorldView aggregates the return values of all the methods in the algebras, and adds a pending field
  * to track unfulfilled requests.
  *
  * @param backlog - work to be done
  * @param agents - the number of agents alive
  * @param managed - the actual agents alive
  * @param alive - listing the agents and how long they've been alive
  * @param pending - I'm not sure why this has a timestamp if they are pending... is it how long they've been pending?
  * @param time
  */

final case class WorldView(
  backlog: Int,
  agents: Int,
  managed: NonEmptyList[MachineNode],
  alive: Map[MachineNode, Epoch],
  pending: Map[MachineNode, Epoch],
  time: Epoch) {

}
