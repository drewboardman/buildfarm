package extractors

import models.{Epoch, MachineNode}
import scalaz._
import scalaz.Scalaz._
import scala.concurrent.duration._
import services.WorldView

/***
  * This gets matched if there is NO BACKLOG of work.
  * What this does is STOPs all nodes that have become stale.
  *
  * This is to save money on the Google container engine
  */

object Stale {
  def unapply(worldView: WorldView): Option[NonEmptyList[MachineNode]] =
    worldView match {
      case WorldView(backlog, _, _, alive, pending, time)
        if alive.nonEmpty =>
        val diff: Map[MachineNode, Epoch] = alive -- pending.keys // all machines that are alive and not pending?
        diff.collect {
          case (n, started) if backlog == 0 && olderThanHour(time, started) => n
          case (n, started) if (time - started) >= 5.hours => n
        }.toList.toNel
      case _ => None
    }

  private def olderThanHour(time: Epoch, started: Epoch) = {
    val diff = (time - started).toMinutes
    diff % 60 >= 58
  }
}
