package extractors

import models.MachineNode
import services.WorldView

/***
  * if there is a backlog of work AND no agents exist AND none are pending to be created
  */

object NeedsAgent {
  def unapply(world: WorldView): Option[MachineNode] =
    world match {
      case WorldView(backlog, 0, managed, alive, pending, _)
        if(
          backlog > 0
            && alive.isEmpty
            && pending.isEmpty
          )
      => Option(managed.head) // Some(null) = Some(null) where as Option(null) = None
      case _ => None
    }
}
