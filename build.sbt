name := "buildfarm"

version := "0.1"

scalaVersion := "2.12.7"

scalacOptions += "-feature"

libraryDependencies += "org.scalaz" %% "scalaz-core" % "7.2.27"
libraryDependencies += "org.scalactic" %% "scalactic" % "3.0.5"
libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.5" % "test"
libraryDependencies += "com.propensive" %% "contextual" % "1.1.0"
